# jenkins-dockerized

Docker configuration for quick simple Jenkins master and slave (docker in docker) set-up

## How to use

### Starting the master server

`docker-compose up -d jenkins`

Open [http://localhost/](http://localhost/) and go through the set-up wizard

### Starting the slave

Then add an agent node in the settings

`env MASTER_URL=<extenal jenkins url (e.g. http://192.168.1.7/)> AGENT_SECRET=<from jenkins master> AGENT_NAME=<agent name> docker-compose up -d slave`